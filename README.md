# How to create a site using [bedrock](https://roots.io/bedrock/)

```
composer create-project roots/bedrock demo.calevans.com

cd demo.calevans.com
git init
```
Rrefresh the VS Code dir

## Examine composer.json
- open composer.json
- talk about 'repositories'
- talk about 'installer paths'
- talk about 'test'

## Install WordPress

ALL DB and WP_SITE vars should already be in the apache config file. If you are storing them in `.env` then copy `.env.sample` to `.env` and populate with the appropriate values.

- Bring up website http://demo.calevans.com
- Do the install
- Log in

## Install some plugins
```
composer require wpackagist-plugin/akismet
composer require wpackagist-plugin/powerpress
composer require wpackagist-theme/podcast
```

## Install some dev tools
```
composer require --dev dealerdirect/phpcodesniffer-composer-installer
composer require --dev phpcompatibility/phpcompatibility-wp
composer require --dev wp-coding-standards/wpcs
```

Copy `.phpcs.cml.dist` over from another project.

## Show working dev environment
```
vendor/bin/phpcs --no-colors web/app/plugins/akismet/
```

## Add a custom plugin

Add this to composer.json

```
{
      "type":"vcs",
      "url":"https://gitlab.com/calevans/s3-podcast-log-cruncher.git"
}
```

Now require it.

```
composer require "calevans/s3-podcast-log-cruncher" ^1.0
```

## Put it in a repo

- Make a repo on gitlab.org
- copy the remote line and paste it into the cli
- `git add .`
- `git commit -am "Initial Checkin"`
- `git push origin master`
- Refresh the repo page in the browser.

## Use it

- Delete the demo.calevans.com directory
- refresh browser page to show it's gone
- `git clone ` repo name demo.calevans.com
- `cd demo.calevans.com`
- `composer insatll`
- refresh browser page to show it's back

